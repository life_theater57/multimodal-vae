# %% [markdown]
# # Variational Autoencoder (VAE) with Keras
# %% [markdown]
# Modified from code source: https://www.machinecurve.com/index.php/2019/12/30/how-to-create-a-variational-autoencoder-with-keras/#full-vae-code
# Modification where inspired in part by : https://www.pyimagesearch.com/2019/02/04/keras-multiple-inputs-and-mixed-data/
# %% [markdown]
# ## Model imports

# %%
import tensorflow as tf
Dataset = tf.data.Dataset

from tensorflow import keras
from tensorflow.keras.layers import Conv2D, Conv2DTranspose, Input, Flatten, Dense, Lambda, Reshape
from tensorflow.keras.layers import BatchNormalization, ZeroPadding2D, Cropping2D, Add
from tensorflow.keras.models import Model
from tensorflow.keras.datasets import mnist
from tensorflow.keras.losses import binary_crossentropy, KLD
from tensorflow.keras import backend as K
from tensorflow.keras import mixed_precision

import numpy as np
import matplotlib.pyplot as plt

import sys

# Following section allow using RTX graphic cards
# Found on StackOverflow: https://stackoverflow.com/questions/57062456/function-call-stack-keras-scratch-graph-error
import tensorflow as tf
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        # Restrict TensorFlow to only use the fourth GPU
        tf.config.experimental.set_visible_devices(gpus[0], 'GPU')

        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)


# %%
# To speed up computation on GPU using tensor cores
#mixed_precision.set_global_policy("mixed_float16")

# %% [markdown]
# ## Model configuration

# %%
# Data & model configuration
batch_size = 128
no_epochs = 100
validation_split = 0.2
verbosity = 1
latent_dim = 2

# %% [markdown]
# ## Preprocessing data
# %% [markdown]
# ### Loading audio data

# %%
# Audio dataframe
from pathlib import Path
import os
import pandas as pd

# Load FSDD dataset
data_path = [
    './datasets/free-spoken-digit-dataset/training-recordings',
    './datasets/free-spoken-digit-dataset/testing-recordings']

def process_filenames(path):
    data_path = Path(path)
    files = data_path.glob('*.wav')

    df = pd.DataFrame(data=files, columns={"filepath"})
    df["filepath"] = df["filepath"].astype(str)
    df["filename"] = df["filepath"].str.split(os.sep, expand=True).iloc[:, -1]
    df[["number", "name", "repetitions"]] = df["filename"].str.split('_', expand=True)

    df["number"] = df["number"].astype(int)
    df["repetitions"] = df["repetitions"].str.split('.').str[0].astype(int)
    return df

audio_df = [process_filenames(path) for path in data_path]

# %% [markdown]
# ### Loading visual data

# %%
# Visual dataframe

# Load MNIST dataset
(input_train, target_train), (input_test, target_test) = mnist.load_data()

# Normalize data
input_train = input_train / 255
input_test = input_test / 255

def generate_visual_df(labels):
    df = pd.DataFrame(data=labels, columns={"number"})
    df = df.reset_index()
    df = df.rename(columns={"index": "image_id"})
    return df

visual_df = [generate_visual_df(labels) for labels in [target_train, target_test]]

# %% [markdown]
# ### Combining modalities

# %%
# Multimodal dataframe
def combine_dataframes(df1, key1, df2, key2):
    # The resulting dataframe will keep key1, but drop key2    
    df1_indices = df1.groupby(key1).indices
    df2_indices = df2.groupby(key2).indices
    
    def merge_dataframes(key):
        if key in df2_indices.keys():
            # Find target length
            l1 = len(df1_indices[key])
            l2 = len(df2_indices[key])
            target_length = max(l1, l2)
            
            # Repeat examples to get target length
            df1_indices_ext = np.repeat(df1_indices[key], np.ceil(target_length / l1))
            df2_indices_ext = np.repeat(df2_indices[key], np.ceil(target_length / l2))

            # Shuffle examples
            df1_indices_ext = np.random.choice(df1_indices_ext, target_length, replace=False)
            df2_indices_ext = np.random.choice(df2_indices_ext, target_length, replace=False)

            # Match examples
            df = df1.iloc[df1_indices_ext].reset_index(drop=True)
            df = df.merge(df2.iloc[df2_indices_ext].drop(key2, axis=1).reset_index(drop=True), left_index=True, right_index=True)

            return df

    return pd.concat([merge_dataframes(key) for key in df1_indices.keys()])

multimodal_df = [combine_dataframes(audio_df[i], "number", visual_df[i], "number") 
                        for i in range(len(audio_df))]

# %% [markdown]
# ### Creating image dataset class

# %%
# Definition
class ImageDataset:
    """This class encloses an image dataset."""    
    def __init__(self, inputs, targets):
        """Initializing the dataset from labeled training and test data."""        
        # Define padding required to get a multiple of 8 as dimensions 
        # (for performances on tensor cores)
        self.set_input_padding(inputs[0])
        
        # Set dataset shape property (identical for all elements in ds)
        self.set_input_shape(inputs[0])
        
        # Cast numbers as floats 
        # (which presumably speeds up the training process)
        inputs = inputs.astype(np.float32)
        targets = targets.astype(np.float32)
        
        # Adjust rank of images if required
        if len(inputs.shape) == 3:
            inputs = np.expand_dims(inputs, axis=3)
        
        # Create Datasets
        self.inputs = Dataset.from_tensor_slices(inputs)   
        self.targets = Dataset.from_tensor_slices(targets)
    
    def labeled_examples(self):
        return (self.inputs, self.targets)

    def set_input_padding(self, image):
        self.input_padding = ((0, 0), (0, 0))
        v_pad = image.shape[0] % 8
        h_pad = image.shape[1] % 8

        if v_pad != 0 or h_pad != 0:
            top_pad = v_pad // 2
            left_pad = h_pad // 2
            self.input_padding = ((top_pad, v_pad - top_pad), 
                                  (left_pad, h_pad - left_pad))

    def set_input_shape(self, image):
        if len(image.shape) == 2:
            height = image.shape[-2]
            width = image.shape[-1]
            num_channels = 1
        elif len(image.shape) > 2:
            height = image.shape[-3]
            width = image.shape[-2]
            num_channels = image.shape[-1]
        else:
            # TODO raise error
            height = image.shape[-2]
            width = image.shape[-1]
            num_channels = 0
            
        self.input_shape = (height, width, num_channels)
        self.num_channels = num_channels

# %% [markdown]
# ### Creating audio dataset class

# %%
# Definition
class AudioDataset(ImageDataset):
    """This class encloses an audio dataset."""    
    def __init__(self, inputs, targets):
        """Initializing the dataset from labeled training and test data."""        
        # Define padding required to get a multiple of 8 as dimensions 
        # (for performances on tensor cores)        
        self.input_padding = ((0, 0), (0, 0))
        
        # Set dataset shape property (identical for all elements in ds)
        image = self.process_path(inputs.iloc[0])
        self.set_input_shape(image.numpy())
        
        # Create datasets
        self.inputs = Dataset.from_tensor_slices(inputs)
        self.targets = Dataset.from_tensor_slices(targets.astype(np.float32))
        
        # Process input dataset
        self.inputs = self.inputs.map(self.process_path)

    def process_path(self, file_path):
        raw_audio = tf.io.read_file(file_path)
        audio, sample_rate = tf.audio.decode_wav(raw_audio)
        audio = tf.reshape(audio, [-1])
        # TODO: config value for frame_length and frame_step
        spectrogram = tf.signal.stft(audio, 512, 128, fft_length=512)
        spectrogram = tf.slice(spectrogram, [0,0], [-1, 128])
        spectrogram = tf.abs(spectrogram)
        spectrogram = tf.square(spectrogram)
        spectrogram = tf.math.l2_normalize(spectrogram, axis=1)
        dim = tf.shape(spectrogram)[0]
        if (dim < 32):
            bottom_pad = tf.cast(tf.math.floor((32 - dim) / 2), tf.int32)
            top_pad = tf.cast(tf.math.ceil((32 - dim) / 2), tf.int32)
            paddings = [[bottom_pad, top_pad], [0, 0]]
            spectrogram = tf.pad(spectrogram, paddings)
        spectrogram = spectrogram[:32,:]

        # Adjust rank to have the spectrogram in format (height, width, channel)
        spectrogram = tf.reshape(spectrogram, [32, 128, 1])
        
        return spectrogram

    def reconstruct_wav(self, spectrogram, sample_rate, sqrt=False):
        spectrogram = tf.square(spectrogram)
        spectrogram = tf.pad(spectrogram, [[0, 0], [0, 129]])
        spectrogram = tf.cast(spectrogram, tf.complex64)
        audio = tf.signal.inverse_stft(spectrogram, 512, 128, fft_length=512)
        audio = tf.reshape(audio, [-1, 1])
        audio = audio * 128
        raw_audio = tf.audio.encode_wav(audio, sample_rate)
        return raw_audio

# %% [markdown]
# ### Creating datasets

# %%
train_count = np.floor(multimodal_df[0]["image_id"].count() * (1 - validation_split)).astype(int)

visual_ds_train = ImageDataset(input_train[multimodal_df[0]["image_id"][:train_count]], multimodal_df[0]["number"][:train_count])
visual_ds_valid = ImageDataset(input_train[multimodal_df[0]["image_id"][train_count:]], multimodal_df[0]["number"][train_count:])
visual_ds_test = ImageDataset(input_test[multimodal_df[1]["image_id"]], multimodal_df[1]["number"])

audio_ds_train = AudioDataset(multimodal_df[0]["filepath"][:train_count], multimodal_df[0]["number"][:train_count])
audio_ds_valid = AudioDataset(multimodal_df[0]["filepath"][train_count:], multimodal_df[0]["number"][train_count:])
audio_ds_test = AudioDataset(multimodal_df[1]["filepath"], multimodal_df[1]["number"])

# %% [markdown]
# ## Creating the encoder
# %% [markdown]
# ### Audio processing definition

# %%
# Definition
i = Input(shape=audio_ds_train.input_shape, name='audio_input')
cx = ZeroPadding2D(padding=audio_ds_train.input_padding)(i)
cx = Conv2D(filters=8, kernel_size=3, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(i)#(cx)
cx = BatchNormalization()(cx)
cx = Conv2D(filters=16, kernel_size=3, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx = BatchNormalization()(cx)
x = Flatten()(cx)
x = Dense(20, activation='relu', kernel_initializer = 'he_normal')(x)
x = BatchNormalization()(x)

# Get Conv2D shape for Conv2DTranspose operation in decoder
audio_conv_shape = K.int_shape(cx)

# %% [markdown]
# ### Visual processing definition

# %%
# Definition
j = Input(shape=visual_ds_train.input_shape, name='visual_input')
cy = ZeroPadding2D(padding=visual_ds_train.input_padding)(j)
cy = Conv2D(filters=8, kernel_size=3, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cy)
cy = BatchNormalization()(cy)
cy = Conv2D(filters=16, kernel_size=3, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cy)
cy = BatchNormalization()(cy)
y = Flatten()(cy)
y = Dense(20, activation='relu', kernel_initializer = 'he_normal')(y)
y = BatchNormalization()(y)

# Get Conv2D shape for Conv2DTranspose operation in decoder
visual_conv_shape = K.int_shape(cy)

# %% [markdown]
# ### Merging layer definition

# %%
m = Add()([x, y])
mu = Dense(latent_dim, name='latent_mu')(m)
sigma = Dense(latent_dim, name='latent_sigma')(m)

# %% [markdown]
# ### Reparameterization trick

# %%
# Define sampling with reparameterization trick
@tf.function
def sample_z(args):
    mu, sigma = args
    batch = K.shape(mu)[0]
    dim = K.int_shape(mu)[1]
    eps = K.random_normal(shape=(batch, dim))
    return mu + tf.exp(0.5 * sigma) * eps

z = Lambda(sample_z, output_shape=(latent_dim, ), name='z')([mu, sigma])

# %% [markdown]
# ### Encoder instantiation

# %%
# Instantiate encoder
encoder = Model([i, j], [mu, sigma, z], name='encoder')
encoder.summary()
keras.utils.plot_model(encoder, 'outputs/multimodal_encoder.png', show_shapes=True)

# %% [markdown]
# ## Creating the decoder
# %% [markdown]
# ### Audio decoder definition

# %%
# Definition
audio_d_i = Input(shape=(latent_dim, ), name='audio_decoder_input')
x = Dense(audio_conv_shape[1] * audio_conv_shape[2] * audio_conv_shape[3], activation='relu', kernel_initializer = 'he_normal')(audio_d_i)
x = BatchNormalization()(x)
x = Reshape((audio_conv_shape[1], audio_conv_shape[2], audio_conv_shape[3]))(x)
cx = Conv2DTranspose(filters=16, kernel_size=3, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(x)
cx = BatchNormalization()(cx)
cx = Conv2DTranspose(filters=8, kernel_size=3, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx = BatchNormalization()(cx)
cx = Conv2DTranspose(filters=audio_ds_train.num_channels, kernel_size=3, activation='sigmoid', padding='same')(cx)
audio_o = Cropping2D(audio_ds_train.input_padding, name='audio_decoder_output')(cx)

# %% [markdown]
# ### Visual decoder definition

# %%
# Definition
visual_d_i = Input(shape=(latent_dim, ), name='visual_decoder_input')
y = Dense(visual_conv_shape[1] * visual_conv_shape[2] * visual_conv_shape[3], activation='relu', kernel_initializer = 'he_normal')(visual_d_i)
y = BatchNormalization()(y)
y = Reshape((visual_conv_shape[1], visual_conv_shape[2], visual_conv_shape[3]))(y)
cy = Conv2DTranspose(filters=16, kernel_size=3, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(y)
cy = BatchNormalization()(cy)
cy = Conv2DTranspose(filters=8, kernel_size=3, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cy)
cy = BatchNormalization()(cy)
cy = Conv2DTranspose(filters=visual_ds_train.num_channels, kernel_size=3, activation='sigmoid', padding='same')(cy)
#TODO: rename sound_input_padding for visual_input_padding
visual_o = Cropping2D(visual_ds_train.input_padding, name='visual_decoder_output')(cy)

# %% [markdown]
# ### Decoder instantiation

# %%
# Instantiate decoder
decoder = Model([audio_d_i, visual_d_i], [audio_o, visual_o], name='decoder')
decoder.summary()
keras.utils.plot_model(decoder, 'outputs/multimodal_decoder.png', show_shapes=True)

# %% [markdown]
# ## Creating the whole VAE

# %%
# Instantiate VAE
vae_outputs = decoder([encoder([i, j])[2], encoder([i, j])[2]])
vae = Model([i, j], vae_outputs, name='vae')
vae.summary()
keras.utils.plot_model(vae, 'outputs/VAE.png', show_shapes=True)

# %% [markdown]
# ## Defining custom VAE loss function

# %%
# Define loss
@tf.function
def kl_reconstruction_loss(true, pred):
    # Reconstruction loss
    reconstruction_loss = binary_crossentropy(K.flatten(true), K.flatten(pred))
    # KL divergence loss
    kl_loss = 1 + sigma - K.square(mu) - K.exp(sigma)
    kl_loss = K.sum(kl_loss, axis=-1)
    kl_loss *= -0.5
    # Total loss = 50% rec + 50% KL divergence loss
    return K.mean(reconstruction_loss + kl_loss)  

# %% [markdown]
# ## Compilation & training

# %%
# Compile with tf optimiser to use tensor cores
opt = tf.keras.optimizers.Adam()
vae.compile(optimizer=opt, loss=[kl_reconstruction_loss, kl_reconstruction_loss])#, experimental_run_tf_function=False)


# %%
# Train autoencoder
vae.fit([audio_ds_train.inputs, visual_ds_train.inputs], 
        [audio_ds_train.inputs, visual_ds_train.inputs], 
        epochs = no_epochs, 
        batch_size = batch_size, 
        validation_data = ([audio_ds_valid.inputs, visual_ds_valid.inputs], [audio_ds_valid.inputs, visual_ds_valid.inputs]))

# %% [markdown]
# ## Visualizing VAE results
# %% [markdown]
# ##### Credits for original visualization code: https://keras.io/examples/variational_autoencoder_deconv/
# ##### (François Chollet).
# %% [markdown]
# ### Visualizing inputs mapped onto latent space

# %%
def viz_latent_space(encoder, data):
    (audio_input_data, audio_target_data), (visual_input_data, visual_target_data) = data
    mu, _, _ = encoder.predict([audio_input_data, visual_input_data])
    plt.figure(figsize=(12.3, 10))
    plt.scatter(mu[:, 0], mu[:, 1], c=target_data)
    plt.xlabel('z - dim 1')
    plt.ylabel('z - dim 2')
    plt.colorbar()
    plt.show()

# %% [markdown]
# ### Visualizing samples from the latent space

# %%
def viz_decoded(encoder, decoder, data):
    num_samples = 15
    figure = np.zeros((img_width * num_samples, img_height * num_samples, num_channels))
    grid_x = np.linspace(-4, 4, num_samples)
    grid_y = np.linspace(-4, 4, num_samples)[::-1]
    for i, yi in enumerate(grid_y):
        for j, xi in enumerate(grid_x):
            z_sample = np.array([[xi, yi]])
            x_decoded = decoder.predict(z_sample)
            digit = x_decoded[0].reshape(img_width, img_height, num_channels)
            figure[i * img_width: (i + 1) * img_width,
                      j * img_height: (j + 1) * img_height] = digit
    plt.figure(figsize=(10, 10))
    start_range = img_width // 2
    end_range = num_samples * img_width + start_range + 1
    pixel_range = np.arange(start_range, end_range, img_width)
    sample_range_x = np.round(grid_x, 1)
    sample_range_y = np.round(grid_y, 1)
    plt.xticks(pixel_range, sample_range_x)
    plt.yticks(pixel_range, sample_range_y)
    plt.xlabel('z - dim 1')
    plt.ylabel('z - dim 2')
    # matplotlib.pyplot.imshow() needs a 2D array, or a 3D array with the third dimension being of shape 3 or 4!
    # So reshape if necessary
    fig_shape = np.shape(figure)
    if fig_shape[2] == 1:
        figure = figure.reshape((fig_shape[0], fig_shape[1]))
    # Show image
    plt.imshow(figure)
    plt.show()

# %% [markdown]
# ### Calling the visualizers

# %%
# Plot results
data = (audio_dataset.test, visual_dataset.test)
viz_latent_space(encoder, data)
#viz_decoded(encoder, decoder, data)

# %% [markdown]
# ## Save & load models
# %% [markdown]
# ### Simple VAE model

# %%
# Save VAE weights
vae.save_weights('models/vae/vae')

# %%
# Exit the program, the code below is for exploration only
exit()


# %%
# Load VAE weights
vae.load_weights('models/vae/vae')

# %% [markdown]
# ### DCGAN-like VAE model

# %%
# Save DCGAN-like architecture models
vae.save_weights('models/dcgan-like/vae')


# %%
# Load DCGAN-like architecture models
vae.load_weights('models/dcgan-like/vae')

# %% [markdown]
# ## DCGAN-like architecture

# %%
no_epochs = 5

# Encoder definition
i       = Input(shape=input_shape, name='encoder_input')
cx = ZeroPadding2D(padding=input_padding)(i)
cx      = Conv2D(filters=128, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx      = BatchNormalization()(cx)
cx      = Conv2D(filters=256, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx      = BatchNormalization()(cx)
cx      = Conv2D(filters=512, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx      = BatchNormalization()(cx)
cx      = Conv2D(filters=1024, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx      = BatchNormalization()(cx)
x       = Flatten()(cx)
x       = Dense(20, activation='relu', kernel_initializer = 'he_normal')(x)
x       = BatchNormalization()(x)
mu      = Dense(latent_dim, name='latent_mu')(x)
sigma   = Dense(latent_dim, name='latent_sigma')(x)

# Get Conv2D shape for Conv2DTranspose operation in decoder
conv_shape = K.int_shape(cx)

# Decoder definition
d_i   = Input(shape=(latent_dim, ), name='decoder_input')
x     = Dense(conv_shape[1] * conv_shape[2] * conv_shape[3], activation='relu', kernel_initializer = 'he_normal')(d_i)
x     = BatchNormalization()(x)
x     = Reshape((conv_shape[1], conv_shape[2], conv_shape[3]))(x)
cx    = Conv2DTranspose(filters=1024, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(x)
cx    = BatchNormalization()(cx)
cx    = Conv2DTranspose(filters=512, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx    = BatchNormalization()(cx)
cx    = Conv2DTranspose(filters=256, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx    = BatchNormalization()(cx)
cx    = Conv2DTranspose(filters=128, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx    = BatchNormalization()(cx)
cx = Conv2DTranspose(filters=num_channels, kernel_size=3, activation='sigmoid', padding='same')(cx)
o = Cropping2D(input_padding, name='decoder_output')(cx)


# %%
# Plot results
data = (input_test, target_test)
viz_latent_space(encoder, data)
viz_decoded(encoder, decoder, data)


# %%
no_epochs = 20


# %%
# Plot results
data = (input_test, target_test)
viz_latent_space(encoder, data)
viz_decoded(encoder, decoder, data)


# %%
'''
  Variational Autoencoder (VAE) with the Keras Functional API.
'''

import keras
from keras.layers import Conv2D, Conv2DTranspose, Input, Flatten, Dense, Lambda, Reshape
from keras.layers import BatchNormalization
from keras.models import Model
from keras.datasets import mnist
from keras.losses import binary_crossentropy
from keras import backend as K
import numpy as np
import matplotlib.pyplot as plt

# Load MNIST dataset
(input_train, target_train), (input_test, target_test) = mnist.load_data()

# Data & model configuration
img_width, img_height = input_train.shape[1], input_train.shape[2]
batch_size = 128
no_epochs = 100
validation_split = 0.2
verbosity = 1
latent_dim = 2
num_channels = 1

# Reshape data
input_train = input_train.reshape(input_train.shape[0], img_height, img_width, num_channels)
input_test = input_test.reshape(input_test.shape[0], img_height, img_width, num_channels)
input_shape = (img_height, img_width, num_channels)

# Parse numbers as floats
input_train = input_train.astype('float32')
input_test = input_test.astype('float32')

# Normalize data
input_train = input_train / 255
input_test = input_test / 255

# # =================
# # Encoder
# # =================

# Definition
i       = Input(shape=input_shape, name='encoder_input')
cx      = Conv2D(filters=8, kernel_size=3, strides=2, padding='same', activation='relu')(i)
cx      = BatchNormalization()(cx)
cx      = Conv2D(filters=16, kernel_size=3, strides=2, padding='same', activation='relu')(cx)
cx      = BatchNormalization()(cx)
x       = Flatten()(cx)
x       = Dense(20, activation='relu')(x)
x       = BatchNormalization()(x)
mu      = Dense(latent_dim, name='latent_mu')(x)
sigma   = Dense(latent_dim, name='latent_sigma')(x)

# Get Conv2D shape for Conv2DTranspose operation in decoder
conv_shape = K.int_shape(cx)

# Define sampling with reparameterization trick
def sample_z(args):
  mu, sigma = args
  batch     = K.shape(mu)[0]
  dim       = K.int_shape(mu)[1]
  eps       = K.random_normal(shape=(batch, dim))
  return mu + K.exp(sigma / 2) * eps

# Use reparameterization trick to ....??
z       = Lambda(sample_z, output_shape=(latent_dim, ), name='z')([mu, sigma])

# Instantiate encoder
encoder = Model(i, [mu, sigma, z], name='encoder')
encoder.summary()

# =================
# Decoder
# =================

# Definition
d_i   = Input(shape=(latent_dim, ), name='decoder_input')
x     = Dense(conv_shape[1] * conv_shape[2] * conv_shape[3], activation='relu')(d_i)
x     = BatchNormalization()(x)
x     = Reshape((conv_shape[1], conv_shape[2], conv_shape[3]))(x)
cx    = Conv2DTranspose(filters=16, kernel_size=3, strides=2, padding='same', activation='relu')(x)
cx    = BatchNormalization()(cx)
cx    = Conv2DTranspose(filters=8, kernel_size=3, strides=2, padding='same',  activation='relu')(cx)
cx    = BatchNormalization()(cx)
o     = Conv2DTranspose(filters=num_channels, kernel_size=3, activation='sigmoid', padding='same', name='decoder_output')(cx)

# Instantiate decoder
decoder = Model(d_i, o, name='decoder')
decoder.summary()

# =================
# VAE as a whole
# =================

# Instantiate VAE
vae_outputs = decoder(encoder(i)[2])
vae         = Model(i, vae_outputs, name='vae')
vae.summary()

# Define loss
def kl_reconstruction_loss(true, pred):
  # Reconstruction loss
  reconstruction_loss = binary_crossentropy(K.flatten(true), K.flatten(pred)) * img_width * img_height
  # KL divergence loss
  kl_loss = 1 + sigma - K.square(mu) - K.exp(sigma)
  kl_loss = K.sum(kl_loss, axis=-1)
  kl_loss *= -0.5
  # Total loss = 50% rec + 50% KL divergence loss
  return K.mean(reconstruction_loss + kl_loss)

# Compile VAE
vae.compile(optimizer='adam', loss=kl_reconstruction_loss)

# Train autoencoder
vae.fit(input_train, input_train, epochs = no_epochs, batch_size = batch_size, validation_split = validation_split)

# =================
# Results visualization
# Credits for original visualization code: https://keras.io/examples/variational_autoencoder_deconv/
# (François Chollet).
# Adapted to accomodate this VAE.
# =================
def viz_latent_space(encoder, data):
  input_data, target_data = data
  mu, _, _ = encoder.predict(input_data)
  plt.figure(figsize=(8, 10))
  plt.scatter(mu[:, 0], mu[:, 1], c=target_data)
  plt.xlabel('z - dim 1')
  plt.ylabel('z - dim 2')
  plt.colorbar()
  plt.show()

def viz_decoded(encoder, decoder, data):
  num_samples = 15
  figure = np.zeros((img_width * num_samples, img_height * num_samples, num_channels))
  grid_x = np.linspace(-4, 4, num_samples)
  grid_y = np.linspace(-4, 4, num_samples)[::-1]
  for i, yi in enumerate(grid_y):
      for j, xi in enumerate(grid_x):
          z_sample = np.array([[xi, yi]])
          x_decoded = decoder.predict(z_sample)
          digit = x_decoded[0].reshape(img_width, img_height, num_channels)
          figure[i * img_width: (i + 1) * img_width,
                  j * img_height: (j + 1) * img_height] = digit
  plt.figure(figsize=(10, 10))
  start_range = img_width // 2
  end_range = num_samples * img_width + start_range + 1
  pixel_range = np.arange(start_range, end_range, img_width)
  sample_range_x = np.round(grid_x, 1)
  sample_range_y = np.round(grid_y, 1)
  plt.xticks(pixel_range, sample_range_x)
  plt.yticks(pixel_range, sample_range_y)
  plt.xlabel('z - dim 1')
  plt.ylabel('z - dim 2')
  # matplotlib.pyplot.imshow() needs a 2D array, or a 3D array with the third dimension being of shape 3 or 4!
  # So reshape if necessary
  fig_shape = np.shape(figure)
  if fig_shape[2] == 1:
    figure = figure.reshape((fig_shape[0], fig_shape[1]))
  # Show image
  plt.imshow(figure)
  plt.show()

# Plot results
data = (input_test, target_test)
viz_latent_space(encoder, data)
viz_decoded(encoder, decoder, data)


# %%
from PIL import Image
from IPython.display import Image as Display
import numpy as np
from matplotlib import pyplot as plt

images = np.zeros((2, img_width, img_height, num_channels))
inputIndex = 12
inputNumber = input_test[inputIndex]
inputTarget = target_test[inputIndex]

# Create input image
imgInput = np.squeeze(inputNumber, axis=2)

# Create output image
data = decoder.predict(encoder.predict(tf.expand_dims(inputNumber, 0)))
data = data[0]
data = np.squeeze(data, axis=2)

# Reconstruct audio
audio = reconstruct_wav(imgInput, 8000)
tf.io.write_file('./outputs/' + str(inputTarget.numpy()) + '_reconstructed_.wav', audio)
audio = reconstruct_wav(data, 8000)
tf.io.write_file('./outputs/' + str(inputTarget.numpy()) + '_reconstructed_predicted.wav', audio)

# Display images
images = (imgInput, data)
    
for im in images:
    plt.figure(figsize=(6,6))
    plt.imshow(tf.transpose(im))
    plt.colorbar()


# %%
inputNumber = input_test[200]

# Display input
imgInput = np.squeeze(inputNumber, axis=2)
imgInput.shape


# %%
in_test[0]


