# README #

This file contains a description of the project and steps necessary to get experimentations running.

### What is this repository for? ###

* This repo contains some experimentations on variational autoencoders implemented in Tensorflow. It is meant to be used as a notebook with VS Code. The main file for now been `VAE_with_Keras_on_images.py`. This is the most recently maintained file that is known to learn well.

### How do I get set up? ###

* Requirements

	The code of the image VAE was tested with 
	
		- Tensorflow 2.5.2
		- CUDA 11.3.1-1
		- cuDNN 8.2.1.32-1+cuda11.3
		
	It should work with
		
		- Tensorflow >=2.5.0, <2.6.0
		- CUDA >=11.2, <11.4
		- cuDNN >=8.1+cuda{cuda_version}, <=8.3+cuda{cuda_version}
		
	See the list of officially tested versions: https://www.tensorflow.org/install/source#gpu
	
	See the procedure to install Tensorflow requirements: https://www.tensorflow.org/install/gpu#install_cuda_with_apt
	
	
* Upgrade pip to version 19 or above and install wheel.
    
	```
	$ pip install -U pip wheel
	```

* Install python dependancies
	
	```
	$ pip install -r requirements.txt
	```

* Open the project in `Visual Studio Code`

### Who do I talk to? ###

* �tienne Boucher (etienneboucher@hotmail.ca)
