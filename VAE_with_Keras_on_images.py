# %% [markdown]
# # Variational Autoencoder (VAE) with Keras
# %% [markdown]
# Modified from code source: https://www.machinecurve.com/index.php/2019/12/30/how-to-create-a-variational-autoencoder-with-keras/#full-vae-code
# %% [markdown]
# ## Model imports

# %%
import tensorflow as tf

from tensorflow import keras
from tensorflow.keras.layers import Layer, Conv2D, Conv2DTranspose, Input, Flatten, Dense, Reshape, BatchNormalization, ZeroPadding2D, Cropping2D
from tensorflow.keras.models import Model
from tensorflow.keras.datasets import mnist
from tensorflow.keras.losses import mean_squared_error
from tensorflow.keras import backend as K
from tensorflow.keras import mixed_precision
from tensorflow.python.framework.ops import disable_eager_execution

import numpy as np
import matplotlib.pyplot as plt

# Following section allow using RTX graphic cards
# Found on StackOverflow: https://stackoverflow.com/questions/57062456/function-call-stack-keras-scratch-graph-error
import tensorflow as tf
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        # Restrict TensorFlow to only use the fourth GPU
        tf.config.experimental.set_visible_devices(gpus[0], 'GPU')

        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)

# The following section allows using mixed precision computation to accelerate
# computation on Nvidia card with computation capabilities 7.0 or higher
if tf.test.is_gpu_available(min_cuda_compute_capability=(7,0)):
    mixed_precision.set_global_policy('mixed_float16')
else:
    print("No Nvidia card with computation capabilities 7.0 or higher has been detected. The 16 bits float mixed precision computation won't be used.")

# Disabling eager execution to train with a custom loss function
disable_eager_execution()

# %% [markdown]
# ## Loading data

# %%
# Load MNIST dataset
(input_train, target_train), (input_test, target_test) = mnist.load_data()

# %% [markdown]
# ## Model configuration

# %%
# Data & model configuration
img_width, img_height = input_train.shape[1], input_train.shape[2]
batch_size = 128
no_epochs = 100
validation_split = 0.2
verbosity = 1
latent_dim = 2
num_channels = 1

# %% [markdown]
# ## Data preprocessing

# %%
# Reshape data
input_train = input_train.reshape(input_train.shape[0], img_height, img_width, num_channels)
input_test = input_test.reshape(input_test.shape[0], img_height, img_width, num_channels)
input_shape = (img_height, img_width, num_channels)

# Parse numbers as floats (which presumably speeds up the training process)
input_train = input_train.astype('float32')
input_test = input_test.astype('float32')

# Normalize data
input_train = input_train / 255
input_test = input_test / 255

# Define padding required to get a multiple of 8 as dimensions 
# (for performances on tensor cores)
input_padding = (0, 0, 0, 0)
v_pad = img_height % 8
h_pad = img_width % 8

if v_pad != 0 or h_pad != 0:
    top_pad = v_pad // 2
    left_pad = h_pad // 2
    input_padding = ((top_pad, v_pad - top_pad), (left_pad, h_pad - left_pad))

# %% [markdown]
# ## Creating the encoder
# %% [markdown]
# ### Encoder definition

# %%
# Definition
i = Input(shape=input_shape, name='encoder_input')
cx = ZeroPadding2D(padding=input_padding)(i)
cx = Conv2D(filters=8, kernel_size=3, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx = BatchNormalization()(cx)
cx = Conv2D(filters=16, kernel_size=3, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx = BatchNormalization()(cx)
x = Flatten()(cx)
x = Dense(20, activation='relu', kernel_initializer = 'he_normal')(x)
x = BatchNormalization()(x)
mu = Dense(latent_dim, name='latent_mu')(x)
log_sigma = Dense(latent_dim, name='latent_sigma')(x)

# Get Conv2D shape for Conv2DTranspose operation in decoder
conv_shape = K.int_shape(cx)

# %% [markdown]
# ### Reparameterization trick

# %%
# Define sampling with reparameterization trick
class ZSampling(Layer):
    def call(self, inputs):
        mu, log_sigma = inputs
        eps = K.random_normal(shape=K.shape(mu), dtype=mu.dtype)
        return mu + tf.exp(log_sigma) * eps
    
    def get_config(self):
        return super(ZSampling, self).get_config()

z = ZSampling(name='z')([mu, log_sigma])

# %% [markdown]
# ### Encoder instantiation

# %%
# Instantiate encoder
encoder = Model(i, [mu, log_sigma, z], name='encoder')
encoder.summary()
#keras.utils.plot_model(encoder, 'broken_deep_model.png', show_shapes=True)

# %% [markdown]
# ## Creating the decoder
# %% [markdown]
# ### Decoder definition

# %%
# Definition
d_i = Input(shape=(latent_dim, ), name='decoder_input')
x = Dense(conv_shape[1] * conv_shape[2] * conv_shape[3], activation='relu', kernel_initializer = 'he_normal')(d_i)
x = BatchNormalization()(x)
x = Reshape((conv_shape[1], conv_shape[2], conv_shape[3]))(x)
cx = Conv2DTranspose(filters=16, kernel_size=3, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(x)
cx = BatchNormalization()(cx)
cx = Conv2DTranspose(filters=8, kernel_size=3, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx = BatchNormalization()(cx)
cx = Conv2DTranspose(filters=num_channels, kernel_size=3, activation='sigmoid', padding='same')(cx)
o = Cropping2D(input_padding, name='decoder_output')(cx)

# %% [markdown]
# ### Decoder instantiation

# %%
# Instantiate decoder
decoder = Model(d_i, o, name='decoder')
decoder.summary()

# %% [markdown]
# ## Creating the whole VAE

# %%
# Instantiate VAE
vae_outputs = decoder(encoder(i)[2])
vae = Model(i, vae_outputs, name='vae')
vae.summary()

# %% [markdown]
# ## Defining custom VAE loss function

# %%
# Define loss
@tf.function
def kl_reconstruction_loss(true, pred):
    # Reconstruction loss
    reconstruction_loss = mean_squared_error(K.flatten(true), K.flatten(pred)) * img_width * img_height
    # KL divergence loss
    kl_loss = 1 + log_sigma - tf.square(mu) - tf.exp(log_sigma)
    kl_loss = tf.reduce_sum(kl_loss, axis=-1)
    kl_loss *= -0.5
    # Total loss = 50% rec + 50% KL divergence loss
    return tf.reduce_mean(reconstruction_loss + kl_loss)
    

# %% [markdown]
# ## Compilation & training

# %%
# Compile with tf optimiser to use tensor cores
opt = tf.keras.optimizers.Adam()
vae.compile(
    optimizer=mixed_precision.LossScaleOptimizer(opt),
    loss=kl_reconstruction_loss, 
    experimental_run_tf_function=False
)


# %%
# Train autoencoder
vae.fit(input_train, input_train, epochs = no_epochs, batch_size = batch_size, validation_split = validation_split)

# %% [markdown]
# ## Visualizing VAE results
# %% [markdown]
# ##### Credits for original visualization code: https://keras.io/examples/variational_autoencoder_deconv/
# ##### (François Chollet).
# %% [markdown]
# ### Visualizing inputs mapped onto latent space

# %%
def viz_latent_space(encoder, data):
    input_data, target_data = data
    mu, _, _ = encoder.predict(input_data)
    plt.figure(figsize=(12.3, 10))
    plt.scatter(mu[:, 0], mu[:, 1], c=target_data)
    plt.xlabel('z - dim 1')
    plt.ylabel('z - dim 2')
    plt.colorbar()
    plt.show()

# %% [markdown]
# ### Visualizing samples from the latent space

# %%
def viz_decoded(encoder, decoder, data):
    num_samples = 15
    figure = np.zeros((img_width * num_samples, img_height * num_samples, num_channels))
    grid_x = np.linspace(-4, 4, num_samples)
    grid_y = np.linspace(-4, 4, num_samples)[::-1]
    for i, yi in enumerate(grid_y):
        for j, xi in enumerate(grid_x):
            z_sample = np.array([[xi, yi]])
            x_decoded = decoder.predict(z_sample)
            digit = x_decoded[0].reshape(img_width, img_height, num_channels)
            figure[i * img_width: (i + 1) * img_width,
                      j * img_height: (j + 1) * img_height] = digit
    plt.figure(figsize=(10, 10))
    start_range = img_width // 2
    end_range = num_samples * img_width + start_range
    pixel_range = np.arange(start_range, end_range, img_width)
    sample_range_x = np.round(grid_x, 1)
    sample_range_y = np.round(grid_y, 1)
    plt.xticks(pixel_range, sample_range_x)
    plt.yticks(pixel_range, sample_range_y)
    plt.xlabel('z - dim 1')
    plt.ylabel('z - dim 2')
    # matplotlib.pyplot.imshow() needs a 2D array, or a 3D array with the third dimension being of shape 3 or 4!
    # So reshape if necessary
    fig_shape = np.shape(figure)
    if fig_shape[2] == 1:
        figure = figure.reshape((fig_shape[0], fig_shape[1]))
    # Show image
    plt.imshow(figure)
    plt.show()

# %% [markdown]
# ### Calling the visualizers

# %%
# Plot results
data = (input_test, target_test)
viz_latent_space(encoder, data)
viz_decoded(encoder, decoder, data)

# %% [markdown]
# ## Compare examples with reconstructed image

# %%
import numpy as np
from matplotlib import pyplot as plt

images = np.zeros((2, img_width, img_height, num_channels))
inputNumber = input_test[10] # <----- Change index here to test another example

# Create input image
imgInput = np.squeeze(inputNumber, axis=2)

# Create output image
data = decoder.predict(encoder.predict(inputNumber.reshape(1, 28, 28, 1)))
data = data[0]
data = np.squeeze(data, axis=2)
# Cast to be sure to convert back to orginal type
data = data.astype(np.float32)

# Display images
images = (imgInput, data)
for im in images:
    plt.figure()
    plt.imshow(im)
    plt.colorbar()

# %% [markdown]
# ## Save & load models
# %% [markdown]
# ### Simple VAE model

# %%
# Save VAE weights
vae.save_weights('models/vae/vae')

# %%
# Exit the program, the code below is for exploration only
exit()


# %%
# Load VAE weights
vae.load_weights('models/vae/vae')

# %% [markdown]
# ## DCGAN-like architecture

# %%
no_epochs = 5

# Encoder definition
i       = Input(shape=input_shape, name='encoder_input')
cx      = ZeroPadding2D(padding=input_padding)(i)
cx      = Conv2D(filters=128, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx      = BatchNormalization()(cx)
cx      = Conv2D(filters=256, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx      = BatchNormalization()(cx)
cx      = Conv2D(filters=512, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx      = BatchNormalization()(cx)
cx      = Conv2D(filters=1024, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx      = BatchNormalization()(cx)
x       = Flatten()(cx)
x       = Dense(20, activation='relu', kernel_initializer = 'he_normal')(x)
x       = BatchNormalization()(x)
mu      = Dense(latent_dim, name='latent_mu')(x)
sigma   = Dense(latent_dim, name='latent_sigma')(x)

# Get Conv2D shape for Conv2DTranspose operation in decoder
conv_shape = K.int_shape(cx)

# Decoder definition
d_i   = Input(shape=(latent_dim, ), name='decoder_input')
x     = Dense(conv_shape[1] * conv_shape[2] * conv_shape[3], activation='relu', kernel_initializer = 'he_normal')(d_i)
x     = BatchNormalization()(x)
x     = Reshape((conv_shape[1], conv_shape[2], conv_shape[3]))(x)
cx    = Conv2DTranspose(filters=1024, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(x)
cx    = BatchNormalization()(cx)
cx    = Conv2DTranspose(filters=512, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx    = BatchNormalization()(cx)
cx    = Conv2DTranspose(filters=256, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx    = BatchNormalization()(cx)
cx    = Conv2DTranspose(filters=128, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx    = BatchNormalization()(cx)
cx    = Conv2DTranspose(filters=num_channels, kernel_size=3, activation='sigmoid', padding='same')(cx)
o     = Cropping2D(input_padding, name='decoder_output')(cx)


# %%
# Plot results
data = (input_test, target_test)
viz_latent_space(encoder, data)
viz_decoded(encoder, decoder, data)


# %%
# Save DCGAN-like architecture models
vae.save_weights('models/dcgan-like/vae')


# %%
# Load DCGAN-like architecture models
vae.load_weights('models/dcgan-like/vae')


