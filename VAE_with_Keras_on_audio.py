# %% [markdown]
# # Variational Autoencoder (VAE) with Keras
# %% [markdown]
# Modified from code source: 
# 
# https://www.machinecurve.com/index.php/2019/12/30/how-to-create-a-variational-autoencoder-with-keras/#full-vae-code
# 
# This experimentation use a modified version of the free-spoken-digit-dataset : 
# 
# https://github.com/Jakobovski/free-spoken-digit-dataset
# %% [markdown]
# ## Model imports

# %%
import tensorflow as tf

from tensorflow import keras
from tensorflow.keras.layers import Conv2D, Conv2DTranspose, Input, Flatten, Dense, Lambda, Reshape
from tensorflow.keras.layers import BatchNormalization, ZeroPadding2D, Cropping2D
from tensorflow.keras.models import Model
from tensorflow.keras.datasets import mnist
from tensorflow.keras.losses import binary_crossentropy, KLD
from tensorflow.keras import backend as K

import numpy as np
import matplotlib.pyplot as plt

# Following section allow using RTX graphic cards
# Found on StackOverflow: https://stackoverflow.com/questions/57062456/function-call-stack-keras-scratch-graph-error
import tensorflow as tf
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        # Restrict TensorFlow to only use the fourth GPU
        tf.config.experimental.set_visible_devices(gpus[0], 'GPU')

        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)

# %% [markdown]
# ## Loading data

# %%
import pathlib
import os
from scipy.io import wavfile

# Load FSDD dataset
data_paths = ('./datasets/free-spoken-digit-dataset/training-recordings', 
              './datasets/free-spoken-digit-dataset/testing-recordings')
data_paths = [pathlib.Path(path) for path in data_paths]
list_data = [tf.data.Dataset.list_files(str(path/'*.wav')) for path in data_paths]

def process_path(file_path):
    label = tf.strings.split(tf.strings.split(file_path, os.sep)[-1], '_')[0]
    raw_audio = tf.io.read_file(file_path)
    audio, sample_rate = tf.audio.decode_wav(raw_audio)
    audio = tf.reshape(audio, [-1])
    # TODO: add config values for frame_length and frame_step
    spectrogram = tf.signal.stft(audio, 512, 128, fft_length=512)
    spectrogram = tf.slice(spectrogram, [0,0], [-1, 128])
    spectrogram = tf.abs(spectrogram)
    spectrogram = tf.square(spectrogram)
    spectrogram = tf.math.l2_normalize(spectrogram, axis=1)
    dim = tf.shape(spectrogram)[0]
    if (dim < 32):
        bottom_pad = tf.cast(tf.math.floor((32 - dim) / 2), tf.int32)
        top_pad = tf.cast(tf.math.ceil((32 - dim) / 2), tf.int32)
        paddings = [[bottom_pad, top_pad], [0, 0]]
        spectrogram = tf.pad(spectrogram, paddings)
    spectrogram = spectrogram[:32,:]
    return spectrogram, label

labeled_data = [data_path.map(process_path) for data_path in list_data]

def reconstruct_wav(spectrogram, sample_rate, sqrt=False):
    spectrogram = tf.square(spectrogram)
    spectrogram = tf.pad(spectrogram, [[0, 0], [0, 129]])
    spectrogram = tf.cast(spectrogram, tf.complex64)
    audio = tf.signal.inverse_stft(spectrogram, 512, 128, fft_length=512)
    audio = tf.reshape(audio, [-1, 1])
    audio = audio * 128
    raw_audio = tf.audio.encode_wav(audio, sample_rate)
    return raw_audio

# Uncomment to generate audio file and check the spectrogram of the reconstructed audio.
'''
for (i, (image, target)) in enumerate(labeled_data[0].take(4)):
    raw_audio = reconstruct_wav(image, 8000)
    tf.io.write_file('./temp/' + str(target.numpy()) + '_reconstructed_' + str(i) + '.wav', raw_audio)
    plt.figure()
    plt.imshow(tf.transpose(image))
    plt.colorbar()
'''


# %%
(labeled_train_data, labeled_test_data) = labeled_data
# Use a great batch count compared to audio files count to use all examples available
input_train, target_train = next(iter(labeled_train_data.batch(20000).repeat(3)))
input_test, target_test = next(iter(labeled_test_data.batch(20000)))

# %% [markdown]
# ## Model configuration

# %%
# Data & model configuration
img_width, img_height = input_train.shape[1], input_train.shape[2]
batch_size = 128
no_epochs = 1000
validation_split = 0.2
verbosity = 1
latent_dim = 2
num_channels = 1

# %% [markdown]
# ## Data preprocessing

# %%
# Reshape data
input_train = tf.reshape(input_train, [input_train.shape[0], img_width, img_height, num_channels])
input_test = tf.reshape(input_test, [input_test.shape[0], img_width, img_height, num_channels])
input_shape = (img_width, img_height, num_channels)

# Parse numbers as floats (which presumably speeds up the training process)
input_train = tf.cast(input_train, tf.float32)
input_test = tf.cast(input_test, tf.float32)

# Define padding required to get a multiple of 8 as dimensions 
# (for better performances on tensor cores)
input_padding = ((0, 0), (0, 0))
v_pad = img_height % 8
h_pad = img_width % 8

if v_pad != 0 or h_pad != 0:
    top_pad = v_pad // 2
    left_pad = h_pad // 2
    input_padding = ((top_pad, v_pad - top_pad), (left_pad, h_pad - left_pad))

# %% [markdown]
# ## Creating the encoder
# %% [markdown]
# ### Encoder definition

# %%
# Definition
i = Input(shape=input_shape, name='encoder_input')
cx = ZeroPadding2D(padding=input_padding)(i)
cx = Conv2D(filters=8, kernel_size=3, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx = BatchNormalization()(cx)
cx = Conv2D(filters=16, kernel_size=3, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx = BatchNormalization()(cx)
x = Flatten()(cx)
x = Dense(20, activation='relu', kernel_initializer = 'he_normal')(x)
x = BatchNormalization()(x)
mu = Dense(latent_dim, name='latent_mu')(x)
sigma = Dense(latent_dim, name='latent_sigma')(x)

# Get Conv2D shape for Conv2DTranspose operation in decoder
conv_shape = K.int_shape(cx)

# %% [markdown]
# ### Reparameterization trick

# %%
# Define sampling with reparameterization trick
@tf.function
def sample_z(args):
    mu, sigma = args
    batch = K.shape(mu)[0]
    dim = K.int_shape(mu)[1]
    eps = K.random_normal(shape=(batch, dim))
    return mu + tf.exp(0.5 * sigma) * eps

z = Lambda(sample_z, output_shape=(latent_dim, ), name='z')([mu, sigma])

# %% [markdown]
# ### Encoder instantiation

# %%
# Instantiate encoder
encoder = Model(i, [mu, sigma, z], name='encoder')
encoder.summary()

# %% [markdown]
# ## Creating the decoder
# %% [markdown]
# ### Decoder definition

# %%
# Definition
d_i = Input(shape=(latent_dim, ), name='decoder_input')
x = Dense(conv_shape[1] * conv_shape[2] * conv_shape[3], activation='relu', kernel_initializer = 'he_normal')(d_i)
x = BatchNormalization()(x)
x = Reshape((conv_shape[1], conv_shape[2], conv_shape[3]))(x)
cx = Conv2DTranspose(filters=16, kernel_size=3, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(x)
cx = BatchNormalization()(cx)
cx = Conv2DTranspose(filters=8, kernel_size=3, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx = BatchNormalization()(cx)
cx = Conv2DTranspose(filters=num_channels, kernel_size=3, activation='sigmoid', padding='same')(cx)
o = Cropping2D(input_padding, name='decoder_output')(cx)

# %% [markdown]
# ### Decoder instantiation

# %%
# Instantiate decoder
decoder = Model(d_i, o, name='decoder')
decoder.summary()

# %% [markdown]
# ## Creating the whole VAE

# %%
# Instantiate VAE
vae_outputs = decoder(encoder(i)[2])
vae = Model(i, vae_outputs, name='vae')
vae.summary()

# %% [markdown]
# ## Defining custom VAE loss function

# %%
# Define loss
@tf.function
def kl_reconstruction_loss(true, pred):
    # Reconstruction loss
    reconstruction_loss = binary_crossentropy(K.flatten(true), K.flatten(pred)) * img_width * img_height
    # KL divergence loss
    kl_loss = 1 + sigma - K.square(mu) - K.exp(sigma)
    kl_loss = K.sum(kl_loss, axis=-1)
    kl_loss *= -0.5
    # Total loss = 50% rec + 50% KL divergence loss
    return K.mean(reconstruction_loss + kl_loss)
    

# %% [markdown]
# ## Compilation & training

# %%
# Compile with tf optimiser to use tensor cores
opt = tf.keras.optimizers.Adam()
opt = tf.train.experimental.enable_mixed_precision_graph_rewrite(opt)
vae.compile(optimizer=opt, loss=kl_reconstruction_loss, experimental_run_tf_function=False)


# %%
# Train autoencoder
vae.fit(input_train, input_train, epochs = no_epochs, batch_size = batch_size, validation_split = validation_split)

# %% [markdown]
# ## Visualizing VAE results
# %% [markdown]
# ##### Credits for original visualization code: https://keras.io/examples/variational_autoencoder_deconv/
# ##### (François Chollet).
# %% [markdown]
# ### Visualizing inputs mapped onto latent space

# %%
def viz_latent_space(encoder, data):
    input_data, target_data = data
    mu, _, _ = encoder.predict(input_data)
    plt.figure(figsize=(12.3, 10))
    plt.scatter(mu[:, 0], mu[:, 1], c=target_data)
    plt.xlabel('z - dim 1')
    plt.ylabel('z - dim 2')
    plt.colorbar()
    plt.show()

# %% [markdown]
# ### Visualizing samples from the latent space
# %% [markdown]
# ### Calling the visualizers

# %%
# Plot results
data = (input_test, target_test)
viz_latent_space(encoder, data)

# %% [markdown]
# ## Compare examples with reconstructed image

# %%
import numpy as np
from matplotlib import pyplot as plt

images = np.zeros((2, img_width, img_height, num_channels))
inputIndex = 12
inputNumber = input_test[inputIndex]
inputTarget = target_test[inputIndex]

# Create input image
imgInput = np.squeeze(inputNumber, axis=2)

# Create output image
data = decoder.predict(encoder.predict(tf.expand_dims(inputNumber, 0)))
data = data[0]
data = np.squeeze(data, axis=2)

# Reconstruct audio
audio = reconstruct_wav(imgInput, 8000)
tf.io.write_file('./outputs/' + str(inputTarget.numpy()) + '_reconstructed_.wav', audio)
audio = reconstruct_wav(data, 8000)
tf.io.write_file('./outputs/' + str(inputTarget.numpy()) + '_reconstructed_predicted.wav', audio)

# Display images
images = (imgInput, data)
    
for im in images:
    plt.figure(figsize=(6,6))
    plt.imshow(tf.transpose(im))
    plt.colorbar()

# %% [markdown]
# ## Save & load models
# %% [markdown]
# ### Simple VAE model

# %%
# Save VAE weights
vae.save_weights('models/vae/vae')

# %%
# Exit the program, the code below is for exploration only
exit()

# %%
# Load VAE weights
vae.load_weights('models/vae/vae')

# %% [markdown]
# ## DCGAN-like architecture

# %%
no_epochs = 5

# Encoder definition
i       = Input(shape=input_shape, name='encoder_input')
cx      = ZeroPadding2D(padding=input_padding)(i)
cx      = Conv2D(filters=128, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx      = BatchNormalization()(cx)
cx      = Conv2D(filters=256, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx      = BatchNormalization()(cx)
cx      = Conv2D(filters=512, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx      = BatchNormalization()(cx)
cx      = Conv2D(filters=1024, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx      = BatchNormalization()(cx)
x       = Flatten()(cx)
x       = Dense(20, activation='relu', kernel_initializer = 'he_normal')(x)
x       = BatchNormalization()(x)
mu      = Dense(latent_dim, name='latent_mu')(x)
sigma   = Dense(latent_dim, name='latent_sigma')(x)

# Get Conv2D shape for Conv2DTranspose operation in decoder
conv_shape = K.int_shape(cx)

# Decoder definition
d_i   = Input(shape=(latent_dim, ), name='decoder_input')
x     = Dense(conv_shape[1] * conv_shape[2] * conv_shape[3], activation='relu', kernel_initializer = 'he_normal')(d_i)
x     = BatchNormalization()(x)
x     = Reshape((conv_shape[1], conv_shape[2], conv_shape[3]))(x)
cx    = Conv2DTranspose(filters=1024, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(x)
cx    = BatchNormalization()(cx)
cx    = Conv2DTranspose(filters=512, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx    = BatchNormalization()(cx)
cx    = Conv2DTranspose(filters=256, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx    = BatchNormalization()(cx)
cx    = Conv2DTranspose(filters=128, kernel_size=5, strides=2, padding='same', activation='relu', kernel_initializer = 'he_normal')(cx)
cx    = BatchNormalization()(cx)
cx    = Conv2DTranspose(filters=num_channels, kernel_size=3, activation='sigmoid', padding='same')(cx)
o     = Cropping2D(input_padding, name='decoder_output')(cx)


# %%
# Plot results
data = (input_test, target_test)
viz_latent_space(encoder, data)


# %%
# Save DCGAN-like architecture models
vae.save_weights('models/dcgan-like/vae')


# %%
# Load DCGAN-like architecture models
vae.load_weights('models/dcgan-like/vae')


